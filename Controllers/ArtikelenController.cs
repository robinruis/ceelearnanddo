﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;

namespace CeeLearnAndDo.Areas.Admin.Controllers
{

    public class ArtikelenController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ArtikelenController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Artikelen
        public IActionResult Index()
        {
            var artikelen = _context.Artikel;
            List<Artikel> artikels = new List<Artikel>();

            foreach (Artikel artikel in artikels)
            {
                Artikel artl = new Artikel
                {
                    ArtikelId = artikel.ArtikelId,
                    Titel = artikel.Titel,
                    Datum = artikel.Datum
                };
                artikels.Add(artl);
            }

            return View(artikelen);
        }

        // GET: Admin/Artikelen/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel
                .FirstOrDefaultAsync(m => m.ArtikelId == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // GET: Admin/Artikelen/Create
        public IActionResult NieuwArtikel()
        {
            return View();
        }

        // POST: Admin/Artikelen/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NieuwArtikel([Bind("ArtikelId,Titel,Categorie,Content,Datum,Publicatie,OmslagAfbeelding")] Artikel artikel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(artikel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(artikel);
        }

        // GET: Admin/Artikelen/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }
            
            return View(artikel);
        }

        // POST: Admin/Artikelen/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ArtikelId,Titel,Categorie,Content,Datum,Publicatie,OmslagAfbeelding")] Artikel artikel)
        {
            if (id != artikel.ArtikelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(artikel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtikelExists(artikel.ArtikelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            
            return View(artikel);
        }

        // GET: Admin/Artikelen/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel

                .FirstOrDefaultAsync(m => m.ArtikelId == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // POST: Admin/Artikelen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var artikel = await _context.Artikel.FindAsync(id);
            _context.Artikel.Remove(artikel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArtikelExists(int id)
        {
            return _context.Artikel.Any(e => e.ArtikelId == id);
        }
    }
}
