﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;

namespace CeeLearnAndDo.Controllers
{
    public class ContactInzendingsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ContactInzendingsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ContactInzendings
        public async Task<IActionResult> Index()
        {
            return View(await _context.ContactInzending.ToListAsync());
        }

        // GET: ContactInzendings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contactInzending = await _context.ContactInzending
                .FirstOrDefaultAsync(m => m.InzendingId == id);
            if (contactInzending == null)
            {
                return NotFound();
            }

            return View(contactInzending);
        }

        // GET: ContactInzendings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ContactInzendings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InzendingId,Naam,Telefoon,Email,Datum,Content")] ContactInzending contactInzending)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contactInzending);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Message = "Succesvol verzonden!";
            return View(contactInzending);
        }

        // GET: ContactInzendings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contactInzending = await _context.ContactInzending.FindAsync(id);
            if (contactInzending == null)
            {
                return NotFound();
            }
            return View(contactInzending);
        }

        // POST: ContactInzendings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InzendingId,Naam,Telefoon,Email,Datum,Content")] ContactInzending contactInzending)
        {
            if (id != contactInzending.InzendingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contactInzending);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContactInzendingExists(contactInzending.InzendingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contactInzending);
        }

        // GET: ContactInzendings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contactInzending = await _context.ContactInzending
                .FirstOrDefaultAsync(m => m.InzendingId == id);
            if (contactInzending == null)
            {
                return NotFound();
            }

            return View(contactInzending);
        }

        // POST: ContactInzendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contactInzending = await _context.ContactInzending.FindAsync(id);
            _context.ContactInzending.Remove(contactInzending);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContactInzendingExists(int id)
        {
            return _context.ContactInzending.Any(e => e.InzendingId == id);
        }
    }
}
