﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CeeLearnAndDo.Controllers
{
    public class ConsultantsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _iweb;

        public ConsultantsController(ApplicationDbContext context, IWebHostEnvironment iweb)
        {
            _context = context;
            _iweb = iweb;
        }

        public IActionResult Index()
        {
            var consultant = _context.Consultant;
            List<Consultant> consultants = new List<Consultant>();

            foreach (Consultant consult in consultants)
            {
                Consultant cst = new Consultant
                {
                    ConsultantId = consult.ConsultantId,
                    Naam = consult.Specialiteiten,
                    Email = consult.Email,
                    Omschrijving = consult.Omschrijving,
                    Profielfoto = consult.Profielfoto
                };
                consultants.Add(cst);
            }

            return View(consultant);
        }

        // GET: Admin/Artikelen/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var consultant = await _context.Consultant
                .FirstOrDefaultAsync(m => m.ConsultantId == id);
            if (consultant == null)
            {
                return NotFound();
            }

            return View(consultant);
        }

    }

}
