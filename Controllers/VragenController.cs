﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CeeLearnAndDo.Controllers
{
    public class VragenController : Controller
    {
        private readonly ApplicationDbContext _context;
        public VragenController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var vragen = _context.Vraag;
            List<Vraag> vraags = new List<Vraag>();

            foreach (Vraag vraag in vraags)
            {
                Vraag vrg = new Vraag
                {
                    VraagId = vraag.VraagId,
                    Titel = vraag.Titel,
                    DatumAangemaakt = vraag.DatumAangemaakt
                };
                vraags.Add(vrg);
            }

            return View(vragen);
        }

        

        [Authorize]
        [HttpGet]
        [Area("Admin")]
        public IActionResult Details(int id)
        {
            var Vraag  = _context.Vraag.Find(id);
            var Reactie = _context.Vraag.Include(v => v.Reacties_Vraag).FirstOrDefault(v => v.VraagId == id);
            return View(Vraag);
        }

        
    }
}