﻿using System;
using System.Collections.Generic;
using System.Text;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CeeLearnAndDo.Data
{
	public class ApplicationDbContext : IdentityDbContext
	{
		public DbSet<Vraag> Vraag { get; set; }
		public DbSet<Consultant> Consultant { get; set; }
		public DbSet<Reactie_Artikel> Reactie_Artikel { get; set; }
		public DbSet<Reactie_Vraag> Reactie_Vraag { get; set; }
		public DbSet<Artikel> Artikel { get; set; }
		//public DbSet<Gebruiker> Gebruiker { get; set; }
		public DbSet<ContactInzending> ContactInzending { get; set; }

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
			
		}

		public ApplicationDbContext()
		{
		}
		/*
	   protected override void OnModelCreating(ModelBuilder modelBuilder)
	   {
		   base.Add(modelBuilder);
		   modelBuilder.Entity<Gebruiker>()
			   .HasMany(p => p.Artikelen)
			   .WithOne()
			   .HasForeignKey(p => p.GebruikerId);

		   modelBuilder.Entity<Artikel>()
			   .HasMany(p => p.Reacties_Artikel)
			   .WithOne()
			   .HasForeignKey(p => p.ArtikelId);

		   modelBuilder.Entity<Gebruiker>()
			   .HasMany(p => p.Reacties_Artikel)
			   .WithOne()
			   .HasForeignKey(p => p.GebruikerId);

		   modelBuilder.Entity<Gebruiker>()
			   .HasMany(p => p.Reacties_Vraag)
			   .WithOne()
			   .HasForeignKey(p => p.GebruikerId);
	   }
	   */
	}
}
