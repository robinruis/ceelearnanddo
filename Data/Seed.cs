﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeeLearnAndDo.Data
{
    public class Seed
    {
        public static void SeedUsers(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (roleManager.FindByNameAsync("Admin").Result == null)
            {
                IdentityRole role = new IdentityRole { Name = "Admin" };
                roleManager.CreateAsync(role).Wait();
            };

            if (roleManager.FindByNameAsync("Gebruiker").Result == null)
            {
                IdentityRole role = new IdentityRole { Name = "Gebruiker" };
                roleManager.CreateAsync(role).Wait();
            };

            if (userManager.FindByEmailAsync("admin@admin.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "admin@admin.com",
                    Email = "admin@admin.com",
                    EmailConfirmed = true
                };
            

            IdentityResult result = userManager.CreateAsync(user, "Password123!").Result;

            if (result.Succeeded)
            {
                userManager.AddToRoleAsync(user, "Admin").Wait();
                userManager.AddToRoleAsync(user, "Gebruiker").Wait();
            }
            }
            if (roleManager.FindByNameAsync("Admin").Result == null)
            {
                IdentityRole role = new IdentityRole { Name = "Admin" };
                roleManager.CreateAsync(role).Wait();
            };

            if (roleManager.FindByNameAsync("Gebruiker").Result == null)
            {
                IdentityRole role = new IdentityRole { Name = "Gebruiker" };
                roleManager.CreateAsync(role).Wait();
            };

            if (userManager.FindByEmailAsync("gebruiker@gebruiker.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "gebruiker@gebruiker.com",
                    Email = "gebruiker@gebruiker.com",
                    EmailConfirmed = true
                };


                IdentityResult result = userManager.CreateAsync(user, "Password123!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Gebruiker").Wait();
                }
            }
        }
    }
}
