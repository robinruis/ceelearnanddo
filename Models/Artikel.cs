﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace CeeLearnAndDo.Models
{
	[Authorize]
	public class Artikel
	{
		[Key]
		public int ArtikelId { get; set; }
		public string Titel { get; set; }
		public string Categorie { get; set; }
		public string Content { get; set; }
		public DateTime Datum { get; set; } = DateTime.Now;
		public bool Publicatie { get; set; }
		public  string OmslagAfbeelding { get; set; }
		public List<Reactie_Artikel> Reacties_Artikel { get; set; }
	}
}
