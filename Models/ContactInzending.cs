﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CeeLearnAndDo.Models
{
	[Table("ContactInzendingens")]
	public class ContactInzending
	{
		[Key]
		public int InzendingId { get; set; }
		[StringLength(100)]
		public string Naam { get; set; }
		
		public int Telefoon { get; set; }
		[StringLength(100)]
		public string Email { get; set; }
		public DateTime Datum { get; set; } = DateTime.Now;
		[StringLength(1000)]
		public string Content { get; set; }
	}
}
