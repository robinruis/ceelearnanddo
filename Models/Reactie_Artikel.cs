﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CeeLearnAndDo.Models
{
	[Authorize]
	public class Reactie_Artikel
	{
		[Key]
		public int ReactieArtikelId { get; set; }
		public string Content { get; set; }
		public bool Publicatie { get; set; }
		public DateTime Datum { get; set; }
		public Artikel Artikel { get; set; }
		public int ArtikelId { get; set; }
	}
}
