﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CeeLearnAndDo.Models
{
	public class Consultant
	{
		public int ConsultantId { get; set; }
		public string Profielfoto { get; set; }
		public string Naam { get; set; }
		public string Email { get; set; }
		public string Specialiteiten { get; set; }
		public string Omschrijving { get; set; }
	}
}
