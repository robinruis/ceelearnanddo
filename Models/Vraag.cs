﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace CeeLearnAndDo.Models
{
	[Authorize]
	public class Vraag
	{
		[Key]
		public int VraagId { get; set; }
		[Display(Name="Onderwerp")]
		[Required(ErrorMessage ="Voer een onderwerp in.")]
		public string Titel { get; set; }
		[Required(ErrorMessage = "Voer een vraag in.")]
		[Display(Name = "Vraag")]
		public string Content { get; set; }

		[Display(Name = "Datum publicatie")]
		[DataType(DataType.Date)]
		public DateTime DatumAangemaakt { get; set; } = DateTime.Now;
		
		[Display(Name = "Datum bijgewerkt")]
		[DataType(DataType.Date)]
		public DateTime DatumBijgewerkt { get; set; } = DateTime.Now;

		[Display(Name = "Toestemming voor publicatie")]
		public bool Toestemming { get; set; }
		
		public List<Reactie_Vraag> Reacties_Vraag { get; set; }
	}
}
