﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CeeLearnAndDo.Models
{
	[Authorize]
	public class Reactie_Vraag
	{
		[Key]
		public int ReactieVraagId { get; set; }
		public string Content { get; set; }
		public DateTime Datum { get; set; }
		public Vraag Vraag { get; set; }
		public int VraagId { get; set; }

	}
}
