﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CeeLearnAndDo.Areas.Admin.Controllers
{
    public class ConsultantController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _iweb;

        public ConsultantController(ApplicationDbContext context, IWebHostEnvironment iweb)
        {
            _context = context;
            _iweb = iweb;
        }
    
        [Authorize]
        [Area("Admin")]
        public IActionResult Index()
        {
            var consultant = _context.Consultant;
            List<Consultant> consultants = new List<Consultant>();

            foreach (Consultant consult in consultants)
            {
                Consultant cst = new Consultant
                {
                    ConsultantId = consult.ConsultantId,
                    Naam = consult.Specialiteiten,
                    Email = consult.Email,
                    Omschrijving = consult.Omschrijving,
                    Profielfoto = consult.Profielfoto
                };
                consultants.Add(cst);
            }

            return View(consultant);
        }

        [Authorize]
        [Area("Admin")]
        public IActionResult NieuweConsultant()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> NieuweConsultant([Bind("ConsultantId,Naam,Specialiteiten,Email,Profielfoto,Omschrijving")] Consultant consultant, IFormFile fileobj)
        {
            if (ModelState.IsValid)
            {

                string imgtxt = Path.GetExtension(fileobj.FileName);
                if (imgtxt == ".jpg" || imgtxt == ".png")
                {
                    var uploadimg = Path.Combine(_iweb.WebRootPath, "Consultants", fileobj.FileName);
                    var fileStream = new FileStream(uploadimg, FileMode.Create);
                    await fileobj.CopyToAsync(fileStream);

                    consultant.Profielfoto = fileobj.FileName;
                    _context.Add(consultant);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.message = "Alleen .jpg of .png afbeeldingen toegestaan.";
                }
            }

            return View(consultant);
        }

        // GET: Admin/Artikelen/Details/5
        [Area("Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var consultant = await _context.Consultant
                .FirstOrDefaultAsync(m => m.ConsultantId == id);
            if (consultant == null)
            {
                return NotFound();
            }

            return View(consultant);
        }
    }

   


}