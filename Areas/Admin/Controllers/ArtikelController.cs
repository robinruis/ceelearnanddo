﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace CeeLearnAndDo.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class ArtikelController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _iweb;

        public ArtikelController(ApplicationDbContext context, IWebHostEnvironment iweb)
        {
            _context = context;
            _iweb = iweb;
        }

        // GET: Admin/Artikelen
        [Authorize]
        [Area("Admin")]
        public IActionResult Index()
        {
            var artikelen = _context.Artikel;
            List<Artikel> artikels = new List<Artikel>();

            foreach (Artikel artikel in artikels)
            {
                Artikel artl = new Artikel
                {
                    ArtikelId = artikel.ArtikelId,
                    Titel = artikel.Titel,
                    Datum = artikel.Datum
                };
                artikels.Add(artl);
            }

            return View(artikelen);
        }

        // GET: Admin/Artikelen/Details/5
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel
                .FirstOrDefaultAsync(m => m.ArtikelId == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // GET: Admin/Artikelen/Create
        [Authorize]
        [Area("Admin")]
        public IActionResult NieuwArtikel()
        {
            return View();
        }
        
        // POST: Admin/Artikelen/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> NieuwArtikel([Bind("ArtikelId,Titel,Categorie,Content,Datum,Publicatie,OmslagAfbeelding")] Artikel artikel, IFormFile fileobj)
        {
            if (ModelState.IsValid)
            {

                string imgtxt = Path.GetExtension(fileobj.FileName);
                string file_name = artikel.ArtikelId.ToString();
                if (imgtxt == ".jpg" || imgtxt == ".png")
                {
                    var uploadimg = Path.Combine(_iweb.WebRootPath, "Omslagfoto", fileobj.FileName);
                    var fileStream = new FileStream(uploadimg, FileMode.Create);
                    await fileobj.CopyToAsync(fileStream);

                    artikel.OmslagAfbeelding = fileobj.FileName;
                    _context.Add(artikel);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.message = "Alleen .jpg of .png afbeeldingen toegestaan.";
                }
            }
            
            return View(artikel);
        }

        // GET: Admin/Artikelen/Edit/5
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> Wijzigen(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }
            
            return View(artikel);
        }

        // POST: Admin/Artikelen/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> Wijzigen(int id, [Bind("ArtikelId,Titel,Categorie,Content,Datum,Publicatie,OmslagAfbeelding")] Artikel artikel)
        {
            if (id != artikel.ArtikelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    _context.Update(artikel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtikelExists(artikel.ArtikelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            
            return View(artikel);
        }

        // GET: Admin/Artikelen/Delete/5
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> Verwijderen(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var artikel = await _context.Artikel

                .FirstOrDefaultAsync(m => m.ArtikelId == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return View(artikel);
        }

        // POST: Admin/Artikelen/Delete/5
        [HttpPost, ActionName("Verwijderen")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Area("Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var artikel = await _context.Artikel.FindAsync(id);
            _context.Artikel.Remove(artikel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        [Area("Admin")]
        private bool ArtikelExists(int id)
        {
            return _context.Artikel.Any(e => e.ArtikelId == id);
        }
    }
}
