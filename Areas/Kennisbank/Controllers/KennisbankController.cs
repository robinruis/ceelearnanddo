﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CeeLearnAndDo.Areas.Kennisbank.Controllers
{
    public class KennisbankController : Controller
    {
        [Area("Kennisbank")]
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }
    }
}