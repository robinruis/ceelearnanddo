﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CeeLearnAndDo.Data;
using CeeLearnAndDo.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CeeLearnAndDo.Areas.Admin.Controllers
{
    public class VragenController : Controller
    {
        private readonly ApplicationDbContext _context;
        public VragenController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Area("Kennisbank")]
        [Authorize]
        public IActionResult Index()
        {
            var vragen = _context.Vraag;
            List<Vraag> vraags = new List<Vraag>();

            foreach (Vraag vraag in vraags)
            {
                Vraag vrg = new Vraag
                {
                    VraagId = vraag.VraagId,
                    Titel = vraag.Titel,
                    DatumAangemaakt = vraag.DatumAangemaakt
                };
                vraags.Add(vrg);
            }

            return View(vragen);
        }

        [Authorize]
        [Area("Kennisbank")]
        public IActionResult NieuweVraag()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [Area("Kennisbank")]
        public IActionResult NieuweVraag(Vraag vraag)
        {
            _context.Add(vraag);
            _context.SaveChanges();
            ViewBag.Message = "De vraag is succesvol ingezonden.";
            return View(vraag);
        }

        [Authorize]
        [HttpGet]
        [Area("Kennisbank")]
        public IActionResult Details(int id)
        {
            var Vraag = _context.Vraag.Find(id);
            var Reactie = _context.Vraag.Include(v => v.Reacties_Vraag).FirstOrDefault(v => v.VraagId == id);

            //IQueryable<string> VraagQuery = from m in _context.Reactie_Vraag where id == Vraag.VraagId;
                                           
            return View(Vraag);
        }

        [HttpPost]
        [Area("Kennisbank")]
        public IActionResult Reactie(Reactie_Vraag model, Vraag vraag)
        {
            _context.Add(model);
            _context.SaveChanges();
            return View(model);
        }
    }
}